---
typora-copy-images-to: ../img
---

Для того чтобы совершнить zero-downtime деплой, изменим файл deploy.yml

```yml
- hosts: web
  tasks:
    - name: 'Start or replace a container with docker_container module'
      docker_container:
        name: "gocalc"
        image: "temanko/gocalc:dec"
        detach: True
        labels:
          app: "gocalc"
        env:
          test1: "seeesdasdsc321123"
        ports:
          - '7000'
      register: docker_results

    - name: 'Retrieve container ID and ephemeral port'
      set_fact:
        container_id: "{{ docker_results.ansible_facts.docker_container.Id }}"
        ephemeral_port: "{{ docker_results.ansible_facts.docker_container.NetworkSettings.Ports['7000/tcp'][0].HostPort }}"

    - block:
      - name: 'Verify that the new container is serving traffic'
        uri:
          url: "http://localhost:{{ ephemeral_port }}/"
          status_code: 200
        register: container_status
        retries: "{{ app.health_retries | default('20') }}"
        delay: "{{ app.health_retry_delay | default('2') }}"
        until: "container_status.status == 200"
      rescue:
      - name: 'Terminate failed container'
        shell: "docker stop {{ container_id }} ; docker rm --force {{ container_id }}"
        ignore_errors: 'yes'

      - fail:
          msg: 'New container failed to return HTTP 200 and has been terminated'

    - name: 'Updating nginx config'
      template:
        src: pass.j2
        dest: /etc/nginx/sites-enabled/pass
      notify: reload nginx

    - name: 'Retrieve container IDs of previous web versions'
      command: "docker ps -qa --no-trunc --filter 'label=app=gocalc'"
      register: previous_containers

    - name: 'Queue previous containers to terminate in 2 minutes'
      at:
        command: "/usr/bin/docker rm --force {{ item }}"
        count: 1
        units: 'minutes'
      with_items: "{{ previous_containers.stdout_lines }}"
      when: (item.find(container_id) == -1)

  become: yes
  become_user: root 
  
  handlers:
  - name: reload nginx
    service:
      name: nginx
      state: reloaded

```

Смысл в том, что Docker, если не указывать порт на который нужно пробрасывать, пробрасывает на случайный порт в системе, после чего Ansible регистрирует параметры деплоя, берет оттуда номер пробросанного порта, переносит этот порт в pass.j2, после чего загружает этот файл на сервер и перезагружает nginx.

Zero-downtime деплой здесь работает из-за того, что при перезагрузке Nginx, он не сразу убивает всех worker-ов, а только тогда, когда завершится последняя сессия с пользователем, а к этому моменту сервис Nginx перезапуститься с новой конфигурацией и уже новые воркеры будут проксировать запросы на другой порт.

Проверим результаты:

![image-20191222201858857](../img/image-20191222201858857.png)

Как видно, у нас получилось осуществить zero-downtime деплой.