---
typora-copy-images-to: ..\img
---

## Создание репозитория

Для начала работы, создадим новый репозиторий.![image-20191218020417689](../img/image-20191218020417689.png)

## Создание `index.html`

После чего добавим файл `index.html` с каким-нибудь наполнением.

![image-20191218020725786](../img/image-20191218020725786.png)

## Создание `.gitlab-ci.yml`

Теперь добавим файл `.gitlab-ci.yml` со следующим наполнением:

```yml
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```

![image-20191218022124416](../img/image-20191218022124416.png)

После чего можно заметить, что конфигурация, которую мы добавили, является корректной.

![image-20191218022318402](../img/image-20191218022318402.png)

И во вкладке pipelines появился наш пайплайн.

![image-20191218022439761](../img/image-20191218022439761.png)

![image-20191218022500287](../img/image-20191218022500287.png)

## URL созданного сайта

Во вкладке Settings-Pages можно найти url нашего сайта.

![image-20191218022620719](../img/image-20191218022620719.png)

![image-20191218022634734](../img/image-20191218022634734.png)

## Проверка результатов

Проверим корректность работы, зайдя на наш веб-сайт.

![image-20191218022746341](../img/image-20191218022746341.png)