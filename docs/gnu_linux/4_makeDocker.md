---
typora-copy-images-to: ../img
---

Теперь нам необходимо создать контейнер с полученным приложением, для этого создадим Dockerfile, в котором опишем Docker-у процесс создания контейнера:

```dockerfile
FROM golang:latest AS build

WORKDIR /go/src/app
COPY . .

ENV CGO_ENABLED=0
RUN go get && go build -o app main.go

FROM alpine:latest

WORKDIR /app
COPY --from=build /go/src/app/app /app/

CMD ["/app/app"]
```

Теперь создадим контейнер:

```
sudo docker build -t temanko/gocalc:dec .
```

![image-20191222180255989](../img/image-20191222180255989.png)

Теперь логинемся в Docker при помощи:

```bash
docker login
```

Просмотрим список доступных images:

![image-20191222181341964](../img/image-20191222181341964.png)

И запушим созданный контейнер в Docker Hub

```
docker push temanko/gocalc:dec
```

![image-20191222181503088](../img/image-20191222181503088.png)

После чего на hub.docker.com можно увидеть наш созданный контейнер

![image-20191222182426354](../img/image-20191222182426354.png)

