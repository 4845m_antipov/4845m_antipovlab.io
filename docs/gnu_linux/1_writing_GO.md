Для создания тестого приложения я выбрал язык go, т.к. он идеально подходит для быстрого прототипирования web-приложений.

Логика данного приложения проста. Пользователь заходит на страницу "example:7000" с указанием двух параметров (чисел). На выходе получаем сумму двух, отправленных чисел.

Исходный код приложения представлен ниже, а так же расположен в git-репозитории: [GitLab](https://gitlab.com/4845m_antipov/gocalc) в файле main.go

```go
package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)


func handler(w http.ResponseWriter, r *http.Request) {
	keys, ok := r.URL.Query()["a"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param 'a' is missing")
		return
	}
	
	a, err :=strconv.Atoi(keys[0])
	if err != nil {
		return
	}
	
	keys, ok = r.URL.Query()["b"]
	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param 'b' is missing")
		return
	}
	
	b, err :=strconv.Atoi(keys[0])
	if err != nil {
		log.Println("Bad number!")
		return
	}
	
	fmt.Fprintf(w, "%d + %d = %d\n", a,b, sum(a,b))
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":7000", nil))
}

func sum (a,b int) int {
	t:=a+b
	return t
}
```

