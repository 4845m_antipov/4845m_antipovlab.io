---
typora-copy-images-to: ../img
---

Для развертывания ПО и настройки машины для их работы была создана утилита Ansible. 

Так как данная утилита использует архитектуру "Клиент-сервер", то для её использования была создана вторая виртуальная машина под управлением ОС Ubuntu 16.04.

```bash
sudo apt-get update;
sudo apt-get install ansible -y;
```

Ansible работает с теми машинами, которые указаны в файле hosts

![image-20191222185915820](../img/image-20191222185915820.png)

> Примечание. Для работы с другими машинами Ansible использует ssh. В связи с этим, между хостовой машиной (где установлен Ansible) и подопечными машинами необходимо настроить, желательно беспарольный, ssh доступ.

Для проверки правильности настройки подключения машин воспользуемся командой:

```bash
ansible -i hosts -m ping all
```

![image-20191222190827532](../img/image-20191222190827532.png)

Теперь создадим ansible-playbook, который нужен для того, чтобы указать Asnible как настраивать подопечные машины, и назовем его deploy.yml.

deploy.yml:

```yml
- hosts: web
  tasks:
    - name: "Installing nginx"
      apt:
        pkg:
        - nginx
        - python3
        - python3-pip
        state: present

    - name: "Installing needed pip plugin for docker with ansible"
      pip:
        name: docker

    - name: "Updating nginx config"
      template:
        src: pass.j2
        dest: /etc/nginx/sites-enabled/pass
      notify: reload nginx

    - name: 'Start or replace a container with docker_container module'
      docker_container:
        name: gocalc
        image: temanko/gocalc:dec
        detach: True
        labels:
          app: "gocalc"
        env:
          test1: "111eadsdsdd"
        ports:
          - '127.0.0.1:7000:7000'

  become: yes
  become_user: root

  handlers:
  - name: reload nginx
    service:
      name: nginx
      state: reloaded
```

pass.j2 (файл конфигурации nginx, который будет копироваться на подопечную машину)

```bash
server {
  listen 80;

  server_name temansky.openit.guap.ru;

  location / {
    proxy_pass http://127.0.0.1:7000;
  }
}
```

Теперь при помощи ansible настроим наш веб-сервер:

```bash
ansible-playbook -i hosts deploy.yml
```

![image-20191222191608180](../img/image-20191222191608180.png)



Проверим работу нашего развернутого веб-сервера:

```bash
curl '192.168.50.10?a=1&b=2'
```

> Примечание. Машина с установленным Ansible имеет ip-адрес 192.168.50.100, а подопечная 192.168.50.10.

![image-20191222191902998](../img/image-20191222191902998.png)

Как видно, наш сервер развернулся правильно.